# **CROSSING THE BORDERS - GMT JAM 2017** #
![test](https://i.imgur.com/dKyVbKH.png)

Crossing the borders is a small videogame made in 48 hours for the first edition of Game Maker's ToolKit Jam. (14/7/17 - 16/7/17).
The project was made in Unity 5 and Clip Studio Paint EX, Photoshop CC 2017, Audacity for art and sound. Free music by Eric Matyas.
 
Jam Theme: https://www.youtube.com/watch?v=i5C1Uj7jJCg

Jam Page: https://itch.io/jam/gmtk-jam

Game Jam Page: https://itch.io/jam/gmtk-jam/rate/158986

Game website: https://bichodust.itch.io/crossing-the-borders


