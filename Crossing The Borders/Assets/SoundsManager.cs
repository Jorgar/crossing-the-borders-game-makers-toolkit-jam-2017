﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManager : MonoBehaviour {

    public List<AudioSource> audioList = new List<AudioSource>();

	// Use this for initialization
	void Start () {
        StartCoroutine(Helicopter());
	}
	
	// Update is called once per frame
	void Update () {
	}

    IEnumerator Helicopter()
    {
        yield return new WaitForSeconds(Random.Range(10, 120));
        audioList[2].Play();
    }
}
