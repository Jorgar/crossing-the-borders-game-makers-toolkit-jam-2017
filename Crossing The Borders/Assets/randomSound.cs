﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomSound : MonoBehaviour {

    private AudioSource audio;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        StartCoroutine(Sound());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Sound()
    {
        yield return new WaitForSeconds(1);
        if(Random.Range(0,200) > 10)
        {
            audio.Play();
        }
    }
}
