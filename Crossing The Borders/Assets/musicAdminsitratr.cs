﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class musicAdminsitratr : MonoBehaviour {

    private AudioSource audio;
    public Toggle togg;
	// Use this for initialization
	void Start () {
        audio = GameObject.Find("Music").gameObject.GetComponent<AudioSource>();

        if (PlayerPrefs.HasKey("volumen"))
        {
            Debug.Log("hola");
            if(PlayerPrefs.GetFloat("volumen") == 0.3f)
            {
                togg.isOn = true;
            }
            else
            {
                togg.isOn = false;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void Silence()
    {
        if (audio.volume == 0.3f)
            audio.volume = 0;
        else if(audio.volume == 0)
            audio.volume = 0.3f;

        PlayerPrefs.GetFloat("volumen", audio.volume);
    }
}
