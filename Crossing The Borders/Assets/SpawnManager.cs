﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public int numberOfEnemies = 200;
    public float intervalSpawner = 30;
    public float sizeWaves = 0.05f;
    public Camera camera;

    public TextMeshProUGUI text;

    public List<GameObject> spawnPoints = new List<GameObject>();
    public GameObject winAnim;
    public GameObject count;

    private bool playerHasLost;
    public bool win { get; set; }

    private void Start()
    {
        playerHasLost = false;
        numberOfEnemies -= GameObject.FindGameObjectsWithTag("Enemy").Length;
        text.text = numberOfEnemies.ToString();
        StartCoroutine(Spawn());
    }

    void Update()
    {
        CheckLost();
        text.text = numberOfEnemies.ToString();
    }

    IEnumerator Spawn()
    {
        if (!playerHasLost)
        {
            int toSpawn = Random.Range(1, 30);

            while (toSpawn != 0)
            {

                int random = Random.Range(0, 7);

                Vector2 positionSpawn = camera.WorldToViewportPoint(spawnPoints[random].GetComponent<Transform>().position);

                Instantiate(Resources.Load("Police"), new Vector3(spawnPoints[random].GetComponent<Transform>().position.x, spawnPoints[random].GetComponent<Transform>().position.y,
                    0), Quaternion.Euler(Vector3.zero));
                toSpawn--;

                if(toSpawn > numberOfEnemies)
                {
                    break;
                }
            }

            yield return new WaitForSeconds(intervalSpawner);
            intervalSpawner += intervalSpawner * 0.05f;
            sizeWaves += sizeWaves * 0.02f;
            StartCoroutine(Spawn());
        }
        
    }

    public void Discount()
    {
        numberOfEnemies--;
        if(numberOfEnemies <= 0)
        {
            numberOfEnemies = 0;
            winAnim.SetActive(true);
            count.SetActive(false);
        }
    }

    void CheckLost()
    {
        if (GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<GmtPlayerManager>().isLost)
        {
            playerHasLost = true;
        }
    }


}
