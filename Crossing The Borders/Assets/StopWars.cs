﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopWars : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("bullet") ||
            collision.gameObject.tag.Equals("bulletEnemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}
