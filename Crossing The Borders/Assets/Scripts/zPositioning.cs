﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zPositioning : MonoBehaviour {

    public SpriteRenderer objectToPosititioning;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals("Player") ||
            collision.gameObject.tag.Equals("Enemy"))
        {
            if (this.gameObject.name.Equals("up"))
            {
                objectToPosititioning.sortingOrder = 999;
            }else if (this.gameObject.name.Equals("down"))
            {
                objectToPosititioning.sortingOrder = -20;
            }
        }


    }
}
