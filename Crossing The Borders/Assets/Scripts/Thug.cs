﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thug : MonoBehaviour {

    public Animator animator;
    public SpriteRenderer headSprite;
    public SpriteRenderer thugSprite;
    public int lives;
    public bool IsDead { get; set; }
    public bool DdThug { get; set;}
    Color initialColor;
    public GameObject weapon;
    private GameObject boss;

    private void Start()
    {
        //animator.SetTrigger("is_killed");
        IsDead = false;
        initialColor = thugSprite.color;
        boss = GameObject.Find("Boss");
    }

    void GetDamage(int damage)
    {
        if (!boss.GetComponent<GmtPlayerManager>().isRunning)
        {
            lives -= damage;
            if (lives == 0)
            {
                animator.SetTrigger("is_killed");
                headSprite.sortingOrder = -11;
                IsDead = true;
                this.gameObject.GetComponent<Transform>().SetParent(null);
                foreach (var boxCollider in this.gameObject.GetComponent<Transform>().GetComponents<BoxCollider2D>())
                {
                    boxCollider.enabled = false;
                }
                weapon.SetActive(false);
                boss.GetComponent<GmtPlayerManager>().Lives -= 1;
                thugSprite.sortingOrder = -7;
            }
            else
            {
                StartCoroutine(Hit());
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("bulletEnemy") && !IsDead)
        {
            GetDamage(1);
            Destroy(collision.gameObject);
        }
    }

    IEnumerator Hit()
    {
        thugSprite.color = new Color(174f / 256, 27 / 256, 27 / 256, 1f);
        yield return new WaitForSeconds(.25f);
        thugSprite.color = initialColor;
    }

}
