﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GmtPlayerManager : MonoBehaviour {

    public float playerVelocity = 3;

    private Vector2 positionToMove;
    private Transform playerTransform;
    private GameObject gun;
    public List<GameObject> weaponsThugs = new List<GameObject>();
    public List<GameObject> slotsThugs = new List<GameObject>();

    private List<GameObject> thugs = new List<GameObject>();

    public int Lives { get; set; }

    public float timeToDestroyBullet = 3;
    bool isRunningInCooldown;

    public bool isLost { get; set; }
    float timer;
    public bool isRunning { get; set; }

    private float initialVelocity;
    GameObject[] heads;

    public GameObject gameOver;

    void Start () {
        heads = GameObject.FindGameObjectsWithTag("head");
        isRunningInCooldown = false;
        isRunning = false;
        isLost = false;
        playerTransform = this.gameObject.GetComponent<Transform>();
        positionToMove = playerTransform.position;
        gun = playerTransform.Find("gun").gameObject;
        initialVelocity = playerVelocity;
        timer = 0;
        foreach (Transform child in playerTransform.GetComponentsInChildren<Transform>())
        {
         
            if (child.gameObject.tag.Equals("weapon"))
            {
                weaponsThugs.Add(child.gameObject);
            }
        }

        foreach (var slot in slotsThugs)
        {
            if (slot.GetComponent<Transform>().childCount > 0)
            {
                thugs.Add(slot.GetComponent<Transform>().GetChild(0).gameObject);
            }
        }

        Lives = thugs.Count;

        Debug.Log(Lives);
        ConfigureThugs();
    }

	void Update () {
        if (!isLost)
        {
            ReadPosition();
            Shoot();
            CheckLives();
            Sprint();
            //Tank();
        }
    }

    private void ReadPosition()
    {
        if (Input.GetKey(KeyCode.Mouse0) && !GameObject.Find("SpawnsPoints").gameObject.GetComponent<SpawnManager>().win)
        {
            //Save the position of the mouse when left click. 
            this.gameObject.GetComponent<Animator>().SetBool("is_walking", true);

            foreach (var childrenAnimators in this.gameObject.GetComponentsInChildren<Animator>())
            {
                childrenAnimators.SetBool("is_walking", true);
            }

            positionToMove = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            //Vector2 direction = ((Vector2)positionToMove - (Vector2)transform.position).normalized; //typo i made was here... normalize instead of normalized

            playerTransform.position = Vector2.MoveTowards(this.gameObject.GetComponent<Transform>().position, positionToMove, playerVelocity * Time.deltaTime);

            //GetComponent<Rigidbody2D>().velocity = direction * playerVelocity;


            if (positionToMove.x < playerTransform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = true;
                foreach (var childrenSpriteRenderer in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (childrenSpriteRenderer.gameObject.tag.Equals("player_thug"))
                    {
                        childrenSpriteRenderer.flipX = true;
                    }
                }
                gun.GetComponent<SpriteRenderer>().flipY = true;
                gun.GetComponent<Transform>().localPosition = new Vector2((gun.GetComponent<Transform>().localPosition.x > 0) ? gun.GetComponent<Transform>().localPosition.x  * - 1 :
                                                            gun.GetComponent<Transform>().localPosition.x , gun.GetComponent<Transform>().localPosition.y);

                foreach (GameObject item in weaponsThugs)
                {
                    item.GetComponent<Transform>().localScale = new Vector2(1,-1);
                }
            }
            else if(positionToMove.x > playerTransform.position.x)
            {
                GetComponent<SpriteRenderer>().flipX = false;
                foreach (var childrenSpriteRenderer in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (childrenSpriteRenderer.gameObject.tag.Equals("player_thug"))
                    {
                        childrenSpriteRenderer.flipX = false;
                    }
                }
                gun.GetComponent<SpriteRenderer>().flipY = false;
                gun.GetComponent<Transform>().localPosition = new Vector2((gun.GetComponent<Transform>().localPosition.x < 0) ? gun.GetComponent<Transform>().localPosition.x * -1 :
                                            gun.GetComponent<Transform>().localPosition.x, gun.GetComponent<Transform>().localPosition.y);

                foreach (GameObject item in weaponsThugs)
                {
                    item.GetComponent<Transform>().localScale = new Vector2(1, 1);
                }
            }

            LookAt2D(gun);

            foreach(GameObject item in weaponsThugs)
            {
                LookAt2D(item);
            }
        }
        else
        {
            this.gameObject.GetComponent<Animator>().SetBool("is_walking", false);
            foreach (var childrenAnimators in this.gameObject.GetComponentsInChildren<Animator>())
            {
                childrenAnimators.SetBool("is_walking", false);
            }

            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }


    private void Shoot()
    {
        if (Input.GetKeyUp(KeyCode.Space) && !isRunning && !GameObject.Find("SpawnsPoints").gameObject.GetComponent<SpawnManager>().win)
        {
            GameObject bullet = (GameObject)Instantiate(Resources.Load("Bullet"));
            bullet.GetComponent<Transform>().SetParent(gun.GetComponent<Transform>(), false);
            Destroy(bullet, timeToDestroyBullet);

            foreach (GameObject weapon in weaponsThugs)
            {
                weapon.GetComponent<AudioSource>().Play();
                GameObject bulletWeapon = (GameObject)Instantiate(Resources.Load("Bullet"));
                bulletWeapon.GetComponent<Transform>().SetParent(weapon.GetComponent<Transform>(), false);
                Destroy(bulletWeapon, timeToDestroyBullet);
            }
        }
    }


    private void LookAt2D(GameObject gameobj)
    {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - gameobj.GetComponent<Transform>().position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        gameobj.GetComponent<Transform>().rotation = Quaternion.Euler(0f, 0f, rot_z);
    }

    public void CheckLives()
    {
        if(Lives == 0)
        {
            this.gameObject.GetComponent<Animator>().SetTrigger("surrender");
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            isLost = true;
            gun.SetActive(false);
            StartCoroutine(showGameOver());
        }

    }


    void ConfigureThugs()
    {
        for (int i = 0;i< slotsThugs.Count; i++)
        {
            switch (i)
            {
                case 0:
                    //slot 1
                    if (slotsThugs[i].GetComponent<Transform>().childCount > 0)
                    {
                        foreach (var spriteRenderer in slotsThugs[i].GetComponent<Transform>().GetComponentsInChildren<SpriteRenderer>())
                        {
                            spriteRenderer.sortingOrder = -3;
                            if (spriteRenderer.gameObject.name.Equals("head") || spriteRenderer.gameObject.name.Equals("weapon"))
                            {
                                spriteRenderer.sortingOrder = -2;
                            }

                        }
                    }
                    break;
                case 1:
                    //slot 2
                    if (slotsThugs[i].GetComponent<Transform>().childCount > 0)
                    {
                        foreach (var spriteRenderer in slotsThugs[i].GetComponent<Transform>().GetComponentsInChildren<SpriteRenderer>())
                        {
                            spriteRenderer.sortingOrder = -3;
                            if (spriteRenderer.gameObject.name.Equals("head") || spriteRenderer.gameObject.name.Equals("weapon"))
                            {
                                spriteRenderer.sortingOrder = -2;
                            }

                        }
                    }
                    break;
                case 2:
                    //slot 3
                    if (slotsThugs[i].GetComponent<Transform>().childCount > 0)
                    {
                        foreach (var spriteRenderer in slotsThugs[i].GetComponent<Transform>().GetComponentsInChildren<SpriteRenderer>())
                        {
                            spriteRenderer.sortingOrder = -4;

                            if (spriteRenderer.gameObject.name.Equals("head") || spriteRenderer.gameObject.name.Equals("weapon"))
                            {
                                spriteRenderer.sortingOrder = -3;
                            }
                        }
                    }
                    break;
                case 3:
                    //slot 4
                    if (slotsThugs[i].GetComponent<Transform>().childCount > 0)
                    {
                        foreach (var spriteRenderer in slotsThugs[i].GetComponent<Transform>().GetComponentsInChildren<SpriteRenderer>())
                        {
                            spriteRenderer.sortingOrder = -5;

                            if (spriteRenderer.gameObject.name.Equals("head") || spriteRenderer.gameObject.name.Equals("weapon"))
                            {
                                spriteRenderer.sortingOrder = -4;
                            }
                        }
                    }
                    break;
                case 4:
                    //slot 5
                    if (slotsThugs[i].GetComponent<Transform>().childCount > 0)
                    {
                        foreach (var spriteRenderer in slotsThugs[i].GetComponent<Transform>().GetComponentsInChildren<SpriteRenderer>())
                        {
                            spriteRenderer.sortingOrder = -5;

                            if (spriteRenderer.gameObject.name.Equals("head") || spriteRenderer.gameObject.name.Equals("weapon"))
                            {
                                spriteRenderer.sortingOrder = -4;
                            }
                        }
                    }
                    break;
            }
        }
    }

    void Sprint()
    {
        if (Input.GetKey(KeyCode.Space) && !isRunningInCooldown)
        {
            timer += Time.deltaTime;

            if (timer > .5f)
            {
                isRunning = true;
                playerVelocity = Mathf.Clamp(initialVelocity * 2, initialVelocity / 2, initialVelocity * 2);
            }

            if (timer > 3f)
            {
                isRunning = false;
                playerVelocity = initialVelocity;
                timer = 0;
                StartCoroutine(CoolDownRunning());
            }
        }
        else if(Input.GetKeyUp(KeyCode.Space))
        {
            timer = 0;

            if (isRunning)
            {
                foreach (var bandit in thugs)
                {
                    bandit.GetComponent<SpriteRenderer>().color = Color.white;
                    
                    bandit.GetComponent<Transform>().Find("head").gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                }
                this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;

                isRunning = false;
                StartCoroutine(CoolDownRunning());
            }

            isRunning = false;

        }
    }

    IEnumerator CoolDownRunning()
    {
        foreach (var bandit in thugs)
        {
            bandit.GetComponent<SpriteRenderer>().color = new Color(1,1, 1, 1);
        }
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1,1, 1, 1);

        foreach (var h in heads)
        {
            h.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }

        isRunningInCooldown = true;
        //playerVelocity = initialVelocity / 2;
        playerVelocity = initialVelocity;
        yield return new WaitForSeconds(3);
        isRunningInCooldown = false;
        playerVelocity = initialVelocity;
    }

    void Tank()
    {
        if (isRunning)
        {
            foreach(var bandit in thugs)
            {
                if (!bandit.GetComponent<Thug>().IsDead)
                {

                    foreach (var h in heads)
                    {
                        h.GetComponent<SpriteRenderer>().color = new Color(0.5f,0.5f,0.5f,0.5f);
                    }
                    bandit.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 0.5f); ;

                }
            }
            this.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 0.5f); ;
        }
    }

    private void LateUpdate()
    {
        if (!isLost)
        {
            Tank();
        }

    }

    IEnumerator showGameOver()
    {
        yield return new WaitForSeconds(1.5f);
        gameOver.SetActive(true);
    }
}
