﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tmgJamBulletManager : MonoBehaviour {

    Rigidbody2D rigidBodyBullet;
    private GameObject startBullet;
    public float velocityBullet = 10;

    private void Start()
    {
        rigidBodyBullet = GetComponent<Rigidbody2D>();
        startBullet = this.gameObject.GetComponent<Transform>().parent.gameObject.GetComponent<Transform>().Find("start_Bullet").gameObject;
        GetComponent<Transform>().position = startBullet.GetComponent<Transform>().position;
        GetComponent<Transform>().SetParent(null);
        rigidBodyBullet.velocity = startBullet.GetComponent<Transform>().right * velocityBullet;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("bulletEnemy") || collision.gameObject.tag.Equals("bullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
