﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tmgPoliceManager : MonoBehaviour {

    private Transform playerTranform;
    private GameObject policeWeapon;
    public GameObject start_Shoot;
    public float timeToDestroyBullet = 3;

    private void Start()
    {
        playerTranform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        policeWeapon = GetComponent<Transform>().Find("police_weapon").gameObject;
    }

    private void Update()
    {
        LookAt2D(policeWeapon);
    }

    private void LookAt2D(GameObject gameobj)
    {
        Vector3 diff = playerTranform.position - gameobj.GetComponent<Transform>().position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        gameobj.GetComponent<Transform>().rotation = Quaternion.Euler(0f, 0f, rot_z);
    }

    private void Shoot()
    {
            GameObject bullet = (GameObject)Instantiate(Resources.Load("Bullet"));
            bullet.GetComponent<Transform>().SetParent(policeWeapon.GetComponent<Transform>(), false);
            Destroy(bullet, timeToDestroyBullet);       
    }
}
