﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TmgJamEnemyManager : MonoBehaviour {

    private Transform playerTranform;
    private Transform enemyTranform;
    private Vector2 positionToMove;
    private bool isInRange;
    public float enemyVelocity = 5f;
    private float distanceToPlayer;

    private GameObject policeWeapon;
    public GameObject start_Shoot;
    public float timeToDestroyBullet = 3;

    public SpriteRenderer spritePolice;
    public SpriteRenderer spriteWeapon;
    public Animator animatiorPolice;

    public float secondsBetweenShoot = 1;

    public int lives = 3;
    Color initialColor;

    private bool isDead;

    private SpawnManager countManager;

    private bool playerHasLost;

    // Use this for initialization
    void Start () {
        countManager = GameObject.Find("SpawnsPoints").GetComponent<SpawnManager>();
        playerTranform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        positionToMove = playerTranform.position;
        enemyTranform = this.gameObject.GetComponent<Transform>();
        isInRange = false;
        isDead = false;
        initialColor = spritePolice.color;
        playerHasLost = false;
        policeWeapon = GetComponent<Transform>().Find("police_weapon").gameObject;
        StartCoroutine(Shoot());
    }
	
	// Update is called once per frame
	void Update () {
        CheckLost();
        if (!isDead && !playerHasLost)
        {
            positionToMove = playerTranform.position;
            Movement();
            LookAt2D(policeWeapon);
            FlipThing();   
        }

    }

    public void Movement()
    {
        if (!isInRange)
        {
            animatiorPolice.SetBool("is_walking", true);

            Vector2 direction = ((Vector2)positionToMove - (Vector2)transform.position).normalized;
            GetComponent<Rigidbody2D>().velocity = direction * enemyVelocity;

            //this.gameObject.GetComponent<Transform>().position = Vector2.MoveTowards(this.gameObject.GetComponent<Transform>().position, positionToMove, enemyVelocity * Time.deltaTime);
        } else if (isInRange && (Vector2.Distance(this.gameObject.GetComponent<Transform>().position, playerTranform.position) < distanceToPlayer * 0.75))
        {
            animatiorPolice.SetBool("is_walking", true);
            Vector3 actualDirection = playerTranform.position - enemyTranform.position;
            Vector3 directionToRunAway = actualDirection;
            directionToRunAway.Normalize();
            directionToRunAway *= -1;
            enemyTranform.position += directionToRunAway * enemyVelocity * Time.deltaTime;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        else
        {
            animatiorPolice.SetBool("is_walking", false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            isInRange = true;
            distanceToPlayer = Vector2.Distance(this.gameObject.GetComponent<Transform>().position, playerTranform.position);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            isInRange = false;
        }

        if (collision.gameObject.tag.Equals("bullet"))
        {
            lives--;
            Destroy(collision.gameObject);
            if(lives <= 0 && !isDead)
            {
                countManager.Discount();
                this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                animatiorPolice.SetTrigger("is_killed");
                isDead = true;
                policeWeapon.SetActive(false);
                StopAllCoroutines();
                foreach (var boxCollider in this.gameObject.GetComponent<Transform>().GetComponents<BoxCollider2D>())
                {
                    boxCollider.enabled = false;
                }
                spritePolice.color = initialColor;
                spritePolice.sortingOrder = -7;
                
            }
            else
            {
                StartCoroutine(Hit());
            }
        }
    }

    private void LookAt2D(GameObject gameobj)
    {
        Vector3 diff = playerTranform.position - gameobj.GetComponent<Transform>().position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        gameobj.GetComponent<Transform>().rotation = Quaternion.Euler(0f, 0f, rot_z);
    }

    public void FlipThing()
    {
        if(playerTranform.position.x < enemyTranform.position.x)
        {
            spritePolice.flipX = true;
            spriteWeapon.flipY = true;
        }else if (playerTranform.position.x > enemyTranform.position.x)
        {
            spritePolice.flipX = false;
            spriteWeapon.flipY = false;
        }
    }

    IEnumerator Shoot()
    {
        if(Vector2.Distance(enemyTranform.position, playerTranform.position) < 20)
        {
            GetComponent<AudioSource>().Play();
            GameObject bullet = (GameObject)Instantiate(Resources.Load("BulletEnemy"));
            bullet.GetComponent<Transform>().SetParent(policeWeapon.GetComponent<Transform>(), false);
            Destroy(bullet, timeToDestroyBullet);
            yield return new WaitForSeconds(secondsBetweenShoot);
        }
        yield return new WaitForEndOfFrame();
        StartCoroutine(Shoot());

    }


    IEnumerator Hit()
    {
        spritePolice.color = new Color(174f / 256, 27 / 256, 27 / 256, 1f);
        yield return new WaitForSeconds(.25f);
        spritePolice.color = initialColor;
    }

    void CheckLost()
    {
        if (playerTranform.gameObject.GetComponent<GmtPlayerManager>().isLost)
        {
            playerHasLost = true;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            StopAllCoroutines();
            animatiorPolice.SetBool("is_walking", false);
        }
    }
}
