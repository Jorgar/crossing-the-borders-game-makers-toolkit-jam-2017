﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GmtJamCameraManager : MonoBehaviour {

    GameObject player;
    Transform cameraTransform;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        cameraTransform = this.gameObject.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        cameraTransform.position = new Vector3(player.GetComponent<Transform>().position.x, player.GetComponent<Transform>().position.y, cameraTransform.position.z);
    }
}
