﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangingScene : MonoBehaviour {

    private void Update()
    {
        if(Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.P))
        {
            Pause();
        }
    }

    public void Start()
    {
        Time.timeScale = 1;
    }

    public void NewGame()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu", LoadSceneMode.Single);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void Pause()
    {
        GameObject.Find("Canvas").gameObject.GetComponent<Transform>().Find("Pause_Options").gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        GameObject.Find("Canvas").gameObject.GetComponent<Transform>().Find("Pause_Options").gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void ShowControls()
    {
        GameObject.Find("Canvas").gameObject.GetComponent<Transform>().Find("Controls").gameObject.SetActive(true);
    }
}
